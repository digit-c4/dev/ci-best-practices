# Best practices for Gitlab CI Pipelines

Gitlab pipelines are a super powerful tool that can quickly enable
automation on single projects. But that ease of use comes with a downside.
Risk is great to accumulate custom automations individually developed, ending
with an unmanageable collection of code that eventually became part of critical processes in the meantime.

The main and most important best practice is therefore to avoid custom pipeline
at any time and promote collection of reusable Gitlab CI jobs.

## Reusable Gitlab CI job
A traditional CI job looks like
```yaml
my-job:
  stage: test
  tags: [lab, docker]
  image: python
  before_script:
    - pip install flake8
  script:
    - flake8 ./source
```
And can be translated into
1. a pre-build Docker image with the `before_script` tasks already executed
   ```Dockerfile
   # my-job.Dockerfile
   FROM python
   RUN pip install flake
   ```
   ```shell
   # Build, tag and publish the image
   docker build -t my-job
   ```
   > bundling as many tasks as possible in the Docker image will heavily decrease the
   > job execution time
2. a yaml file with the job block, using the Docker image
   ```yaml
   .my-job-template:
      image: code.europa.eu:4567/digit-c4/dev/ci-best-practices/my-job
      before_script:  # nothing, it's now in the Docker image
      script:
        # make it flexible, give users a way to tune the pipeline
        # but refer to best practices for default values
        - flake8 ${SRC_PATH:=./src}
   ```
3. And, finally, a project `.gitlab-ci.yaml`, including the task template
   ```yaml
   include:
   - project: 'digit-c4/dev/ci-best-practices'
     file: 'gitlab-ci/my-job.yml'
     ref: v1.0
   
   other-job:
     extends: .my-job-template
     stage: test
     tags: [lab, docker]
     variables:
       SRC_PATH: ./source
   ```

## Project skeleton and naming conventions
* Gitlab job templates should be collected in `x-best-practices` repositories in a
`/gitlab-ci` folder.
* the job name should be kebab case, ending with `-template` suffix
* the yaml file should be the job name, but without the suffix
* the Dockerfile should be the job name, without the suffix, ending with `.Dockerfile`
* the pipelines documented either in root `README.md` or `gitlab-ci/README.md`
```
x-best-practices
|- gitlab-ci/
| |- my-job.yml         <= declares `.my-job-template` job
| |- my-job-tests.yml   <= see testing section
| |- my-job.Dockerfile  <= published as `my-job` Docker image
| |- README.md
|- .gitlab-ci.yml
|- README.md
```

## Testing
Testing a Gitlab is a tedious job and this section aims to make it easier.

### Static testing
The first and simplest test is to enable basic yaml linting.
Most IDE can be configured to check the formatting, and
```bash
python3 -m venv venv && source venv/bin/activate
pip install yamllint
yamllint ./gitlab-ci/
```

### Distant end-to-end testing
The most representative way of testing a pipeline is unfortunately still the actual execution on a Gitlab runner.
A way to achieve it is
1. To create a `X-tests.yml` file to list the tests for a given job block
   ```yaml
   # /gitlab-ci/my-job-tests.yml
   include:
     - local: /gitlab-ci/my-job.yml
   
   test-my-job-basic:
     extends: .my-job-template
     stage: test
     tags: [lab, shell]
     rules:
     # Only run when there is a change into a job related file
     - changes: [gitlab-ci/my-job*.yml]
     # Test with a basic set of vars
     variables:
       REQUIRED_VAR: 
     before_script:
       - do_anything_to_prepare_the_test
     after_script:
       - do_anything_to_clean_the test
   
   test-my-job-complex:
     extends: .my-job-template
     stage: test
     tags: [lab, shell]
     rules:
     # Only run when there is a change into a job related file
     - changes: [gitlab-ci/my-job*.yml]
     variables:
       REQUIRED_VAR: value
       OTHER_VAR: "12"
     before_script:
       - do_anything_to_prepare_the_test
     after_script:
       - do_anything_to_clean_the test
   ```
2. and to import that test file into the `.gitlab-ci.yml` file of the `x-best-practices` project
   ```yaml
   stages:
   - test

   include:
   - local: /gitlab-ci/my-job-tests.yml
   ```

## Pipelines

### Build job docker image
a job to be executed on a [`shell` runner](https://docs.gitlab.com/runner/executors/shell.html)
to build a Gitlab CI job docker image.

```yaml
include:
   - project: 'digit-c4/dev/ci-best-practices'
     file: 'gitlab-ci/build-job-docker-image.yml'
     ref: b1

build-job-docker-image:
  extends: .build-job-docker-image-template
  stage: build
  variables:
    # Required, job name without -template suffix
    JOB_NAME: "my-job"
    # "true" or "false", whether the commit sha should be used as image tag. Default is "true"
    DOCKER_TAG_SHA: "true"
    # "true" or "false", whether the image should be tagged as latest. Default is "false"
    DOCKER_TAG_LATEST: "false"
    # A comma separated list of tags to assigned to the image
    DOCKER_TAGS: ""
    # Default is "gitlab-ci/${JOB_NAME}.Dockerfile"
    DOCKERFILE_PATH: gitlab-ci/my-job.Dockerfile
    # Default is "."
    DOCKER_CONTEXT_PATH: .
```
